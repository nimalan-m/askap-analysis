// This is an automatically generated file. Please DO NOT edit!
/// @file
///
/// Package config file. ONLY include in ".cc" files never in header files!

// std include
#include <string>

#ifndef ASKAP_ANALYSIS_H
#define ASKAP_ANALYSIS_H

  /// The name of the package
#define ASKAP_PACKAGE_NAME "analysis"

/// askap namespace
namespace askap {
  /// @return version of the package
  std::string getAskapPackageVersion_analysis();
}
  /// The version of the package
#define ASKAP_PACKAGE_VERSION askap::getAskapPackageVersion_analysis() 

#endif
