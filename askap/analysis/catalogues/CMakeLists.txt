#
# askap/analysis/catalogues
#
add_sources_to_analysis(
AbsorptionCatalogue.cc
Casda.cc
CasdaObject.cc
CasdaCatalogue.cc
CasdaAbsorptionObject.cc
CasdaComponent.cc
CasdaHiEmissionObject.cc
CasdaIsland.cc
CasdaPolarisationEntry.cc
ComponentCatalogue.cc
FitCatalogue.cc
HiEmissionCatalogue.cc
IslandCatalogue.cc
RMCatalogue.cc
)

install (FILES
AbsorptionCatalogue.h
Casda.h
CasdaObject.h
CasdaCatalogue.h
CasdaAbsorptionObject.cc
CasdaAbsorptionObject.h
CasdaComponent.h
CasdaHiEmissionObject.h
CasdaIsland.h
CasdaPolarisationEntry.h
ComponentCatalogue.h
FitCatalogue.h
HiEmissionCatalogue.h
IslandCatalogue.h
RMCatalogue.h

DESTINATION include/askap/analysis/catalogues
)

