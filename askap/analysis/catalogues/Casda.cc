/// @file
///
/// Constants needed for CASDA catalogues
///
/// @copyright (c) 2014 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/catalogues/Casda.h>
#include <Blob/BlobIStream.h>
#include <Blob/BlobOStream.h>
#include <askap/analysis/sourcefitting/RadioSource.h>
#include <askap/analysis/outputs/CataloguePreparation.h>
#include <Common/ParameterSet.h>

namespace askap {

namespace analysis {

namespace casda {

std::string getIslandID(sourcefitting::RadioSource &obj)
{
    std::stringstream id;
    id << "island_" << obj.getID();
    return id.str();
}

std::string getComponentID(sourcefitting::RadioSource &obj, const unsigned int fitNumber)
{
    std::stringstream id;
    id << "component_" << obj.getID() << getSuffix(fitNumber);
    return id.str();
}


ValueError::ValueError():
    itsValue(0.),
    itsError(0.)
{
}

ValueError::ValueError(double val, double err):
    itsValue(val),
    itsError(err)
{
}

ValueError::~ValueError()
{
}

LOFAR::BlobOStream& operator<<(LOFAR::BlobOStream &blob, ValueError& src)
{
    blob << src.itsValue;
    blob << src.itsError;
    return blob;
}

LOFAR::BlobIStream& operator>>(LOFAR::BlobIStream &blob, ValueError& src)
{
    blob >> src.itsValue;
    blob >> src.itsError;
    return blob;
}


PrecisionManager::PrecisionManager():
    itsFlux(casda::precFlux),
    itsFreqCont(casda::precFreqContinuum),
    itsFreqSpec(casda::precFreqSpectral),
    itsVelSpec(casda::precVelSpectral),
    itsZ(casda::precZ),
    itsWidthSpec(casda::precSpecWidth),
    itsSize(casda::precSize),
    itsShapeSpec(casda::precSpecShape),
    itsPos(casda::precPos),
    itsPix(casda::precPix),
    itsLamsq(casda::precLamsq),
    itsFD(casda::precFD),
    itsAngle(casda::precAngle),
    itsPfrac(casda::precPfrac),
    itsStats(casda::precStats),
    itsSolidangle(casda::precSolidangle)
{
}

PrecisionManager::PrecisionManager(const LOFAR::ParameterSet &parset):
    itsFlux(parset.getUint("precFlux",casda::precFlux)),
    itsFreqCont(parset.getUint("precFreqCont",casda::precFreqContinuum)),
    itsFreqSpec(parset.getUint("precFreqSpec",casda::precFreqSpectral)),
    itsVelSpec(parset.getUint("precVel",casda::precVelSpectral)),
    itsZ(parset.getUint("precZ",casda::precZ)),
    itsWidthSpec(parset.getUint("precWidth",casda::precSpecWidth)),
    itsSize(parset.getUint("precSize",casda::precSize)),
    itsShapeSpec(parset.getUint("precShape",casda::precSpecShape)),
    itsPos(parset.getUint("precPos",casda::precPos)),
    itsPix(parset.getUint("precPix",casda::precPix)),
    itsLamsq(parset.getUint("precLamsq",casda::precLamsq)),
    itsFD(parset.getUint("precFD",casda::precFD)),
    itsAngle(parset.getUint("precAngle",casda::precAngle)),
    itsPfrac(parset.getUint("precPfrac",casda::precPfrac)),
    itsStats(parset.getUint("precStats",casda::precStats)),
    itsSolidangle(parset.getUint("precSolidangle",casda::precSolidangle))
{
}



}
}
}

