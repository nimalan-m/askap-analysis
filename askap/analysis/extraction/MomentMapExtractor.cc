/// @file
///
/// Extraction from a spectral cube to make moment maps of a detection
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/MomentMapExtractor.h>
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <string>
#include <iostream>
#include <sstream>

#include <askap/analysis/sourcefitting/RadioSource.h>

#include <askap/imageaccess/ImageAccessFactory.h>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/ArrayLogical.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/images/Images/ImageInterface.h>
#include <casacore/images/Images/ImageOpener.h>
#include <casacore/images/Images/FITSImage.h>
#include <casacore/images/Images/MIRIADImage.h>
#include <casacore/images/Images/SubImage.h>
#include <casacore/coordinates/Coordinates/CoordinateUtil.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/coordinates/Coordinates/SpectralCoordinate.h>
#include <casacore/coordinates/Coordinates/StokesCoordinate.h>
#include <casacore/measures/Measures/Stokes.h>

#include <duchamp/PixelMap/Voxel.hh>

#include <Common/ParameterSet.h>

using namespace askap::analysis::sourcefitting;

ASKAP_LOGGER(logger, ".momentmapextractor");



namespace askap {

namespace analysis {

MomentMapExtractor::MomentMapExtractor(const LOFAR::ParameterSet& parset):
    SourceDataExtractor(parset)
{
    itsSpatialMethod = parset.getString("spatialMethod", "box");
    if (itsSpatialMethod != "fullfield" && itsSpatialMethod != "box") {
        ASKAPLOG_WARN_STR(logger,
                          "The value of spatialMethod='" << itsSpatialMethod <<
                          "' is not recognised - setting spatialMethod='box'");
        itsSpatialMethod = "box";
    }
    itsFlagUseDetection = parset.getBool("useDetectedPixels", true);

    itsPadSize = parset.getUint("padSize", 5);

    itsOutputFilenameBase = parset.getString("momentOutputBase", "");

    for (int i = 0; i < 3; i++) {
        itsMomentRequest[i] = false;
    }
    std::vector<int> request = parset.getIntVector("moments", std::vector<int>(1, 0));
    bool haveDud = false;
    for (size_t i = 0; i < request.size(); i++) {
        if (request[i] < 0 || request[i] > 2) {
            haveDud = true;
        }
        else {
            itsMomentRequest[i] = true;
        }
    }
    std::vector<int> momentsUsed;
    for (int i = 0; i < 3; i++) {
        if (itsMomentRequest[i]) {
            momentsUsed.push_back(i);
        }
    }
    if (haveDud) {
        ASKAPLOG_WARN_STR(logger,
                          "You requested invalid moments. Only doing " <<
                          casa::Vector<Int>(momentsUsed));
    }
    else {
        ASKAPLOG_INFO_STR(logger,
                          "Will compute the following moments " <<
                          casa::Vector<Int>(momentsUsed));
    }

    itsMom0map = casa::Array<Float>();
    itsMom1map = casa::Array<Float>();
    itsMom2map = casa::Array<Float>();

}

void MomentMapExtractor::defineSlicer()
{

    if (this->openInput() && itsSourceInBounds) {
        casa::IPosition blc(itsInputShape.size(), 0);
        casa::IPosition trc = itsInputShape - 1;

        long zero = 0;
        blc(itsSpcAxis) = std::max(zero, itsSource->getZmin() - 3 + itsSource->getZOffset());
        trc(itsSpcAxis) = std::min(itsInputShape(itsSpcAxis) - 1, itsSource->getZmax() + 3 + itsSource->getZOffset());

        if (itsSpatialMethod == "box") {
            blc(itsLngAxis) = std::max(zero,
                                       itsSource->getXmin() - itsPadSize + itsSource->getXOffset());
            blc(itsLatAxis) = std::max(zero,
                                       itsSource->getYmin() - itsPadSize + itsSource->getYOffset());
            trc(itsLngAxis) = std::min(itsInputShape(itsLngAxis) - 1,
                                       itsSource->getXmax() + itsPadSize + itsSource->getXOffset());
            trc(itsLatAxis) = std::min(itsInputShape(itsLatAxis) - 1,
                                       itsSource->getYmax() + itsPadSize + itsSource->getYOffset());
            /// @todo Not yet dealing with Stokes axis properly.
        }
        else if (itsSpatialMethod == "fullfield") {
            // Don't need to do anything here, as we use the Slicer
            // based on the full image itsInputShape.
        }
        else {
            ASKAPTHROW(AskapError,
                       "Incorrect value for method ('" <<
                       itsSpatialMethod << "') in cube cutout");
        }

        itsSlicer = casa::Slicer(blc, trc, casa::Slicer::endIsLast);
        ASKAPLOG_DEBUG_STR(logger, itsSlicer);
        this->initialiseArray();
    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

casa::IPosition MomentMapExtractor::arrayShape()
{
    int lngsize = itsSlicer.length()(itsLngAxis);
    int latsize = itsSlicer.length()(itsLatAxis);
    casa::IPosition shape(4, lngsize, latsize, 1, 1);
    return shape;
}

void MomentMapExtractor::initialiseArray()
{
    if (this->openInput()) {
        casa::IPosition shape = this->arrayShape();
        itsArray = casa::Array<Float>(shape, 0.0f);
    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void MomentMapExtractor::extract()
{
    this->defineSlicer();
    if (this->openInput() && itsReadyToExtract) {

        ASKAPLOG_INFO_STR(logger,
                          "Extracting moment map from " << itsInputCube <<
                          " surrounding source ID " << itsSourceID <<
                          " with slicer " << itsSlicer);

        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        casa::Array<Float> subarray(ia->read(itsInputCube, itsSlicer.start(), itsSlicer.end()));
        casa::LogicalArray submask(ia->readMask(itsInputCube, itsSlicer.start(), itsSlicer.end()));

        if (itsMomentRequest[0]) {
            this->getMom0(subarray, submask);
        }
        if (itsMomentRequest[1]) {
            this->getMom1(subarray, submask);
        }
        if (itsMomentRequest[2]) {
            this->getMom2(subarray, submask);
        }

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

void MomentMapExtractor::writeImage()
{

    itsInputCube = itsInputCubeList[0];
    if (this->openInput() && itsSourceInBounds && itsReadyToExtract) {
        casa::CoordinateSystem newcoo = casa::CoordinateUtil::defaultCoords4D();

        int dirCoNum = itsInputCoords.findCoordinate(casa::Coordinate::DIRECTION);
        int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
        int stkCoNum = itsInputCoords.findCoordinate(casa::Coordinate::STOKES);

        casa::DirectionCoordinate dircoo(itsInputCoords.directionCoordinate(dirCoNum));
        casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
        casa::Vector<Int> stkvec(itsStokesList.size());
        for (size_t i = 0; i < stkvec.size(); i++) {
            stkvec[i] = itsStokesList[i];
        }
        casa::StokesCoordinate stkcoo(stkvec);

        newcoo.replaceCoordinate(dircoo, newcoo.findCoordinate(casa::Coordinate::DIRECTION));
        newcoo.replaceCoordinate(spcoo, newcoo.findCoordinate(casa::Coordinate::SPECTRAL));
        if (stkCoNum >= 0) {
            newcoo.replaceCoordinate(stkcoo, newcoo.findCoordinate(casa::Coordinate::STOKES));
        }

        int lngAxis = newcoo.directionAxesNumbers()[0];
        int latAxis = newcoo.directionAxesNumbers()[1];
        int stkAxis = newcoo.polarizationAxisNumber();
        casa::IPosition outshape(4, 1);
        outshape(lngAxis) = itsSlicer.length()(itsLngAxis);
        outshape(latAxis) = itsSlicer.length()(itsLatAxis);
        outshape(stkAxis) = stkvec.size();
        if (itsSpatialMethod == "box") {
            // shift the reference pixel for the spatial coords, so
            // that the RA/DEC (or whatever) are correct. Leave the
            // spectral/stokes axes untouched.  only want to do this
            // if we are trimming.
            casa::Vector<Float> shift(outshape.size(), 0);
            casa::Vector<Float> incrFac(outshape.size(), 1);
            shift(lngAxis) = itsSource->getXmin() - itsPadSize + itsSource->getXOffset();
            shift(latAxis) = itsSource->getYmin() - itsPadSize + itsSource->getYOffset();
            casa::Vector<Int> newshape = outshape.asVector();
            newcoo.subImageInSitu(shift, incrFac, newshape);
        }

        for (int i = 0; i < 3; i++) {
            if (itsMomentRequest[i]) {

                casa::LogicalArray theMask;
                std::string newunits;
                switch (i) {
                    case 0:
                        itsArray = itsMom0map;
                        theMask = itsMom0mask.reform(outshape);
                        if (spcoo.restFrequency() > 0.) {
                            newunits = casacore::String(itsInputUnits) + " " +
                                       spcoo.velocityUnit();
                        }
                        else {
                            newunits = casacore::String(itsInputUnits) + " " +
                                       spcoo.worldAxisUnits()[0];
                        }
                        break;
                    case 1:
                        itsArray = itsMom1map;
                        theMask = itsMom1mask.reform(outshape);
                        if (spcoo.restFrequency() > 0.) {
                            newunits = spcoo.velocityUnit();
                        }
                        else {
                            newunits = spcoo.worldAxisUnits()[0];
                        }
                        break;
                    case 2:
                        itsArray = itsMom2map;
                        theMask = itsMom2mask.reform(outshape);
                        if (spcoo.restFrequency() > 0.) {
                            newunits = spcoo.velocityUnit();
                        }
                        else {
                            newunits = spcoo.worldAxisUnits()[0];
                        }
                        break;
                }

                Array<Float> newarray(itsArray.reform(outshape));

                std::string filename = this->outfile(i);
                ASKAPLOG_INFO_STR(logger, "Writing moment-" << i << " map to '" <<
                                  filename << "'");
                boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
                ia->create(filename, newarray.shape(), newcoo);

                // write the array
                ia->write(filename, newarray);

                ia->setUnits(filename, newunits);

                this->writeBeam(filename);
                updateHeaders(filename);

                ia->makeDefaultMask(filename);
                ia->writeMask(filename, theMask, casa::IPosition(outshape.nelements(), 0));

            }
        }

    }
    else {
        ASKAPLOG_ERROR_STR(logger, "Could not open image");
    }
}

std::string MomentMapExtractor::outfile(int moment)
{
    std::stringstream ss;
    ss << moment;
    std::string filename = itsOutputFilename;
    size_t loc;
    while (loc = filename.find("%m"),
            loc != std::string::npos) {
        filename.replace(loc, 2, ss.str());
    }
    return filename;
}

double MomentMapExtractor::getSpectralIncrement()
{
    double specIncr;
    int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
    casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
    if (spcoo.restFrequency() > 0.) {
        // can convert to velocity
        double vel1, vel2;
        spcoo.pixelToVelocity(vel1, 0);
        spcoo.pixelToVelocity(vel2, 1);
        specIncr = fabs(vel1 - vel2);
    }
    else {
        // can't do velocity conversion, so just use the WCS spectral units
        specIncr = fabs(spcoo.increment()[0]);
    }
    return specIncr;
}

double MomentMapExtractor::getSpectralIncrement(int z)
{
    int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
    casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
    double specIncr;
    if (spcoo.restFrequency() > 0.) {
        casa::Quantum<Double> vel, velMinus, velPlus;
        ASKAPASSERT(spcoo.pixelToVelocity(vel, double(z)));
        ASKAPASSERT(spcoo.pixelToVelocity(velMinus, double(z - 1)));
        ASKAPASSERT(spcoo.pixelToVelocity(velPlus, double(z + 1)));
//        ASKAPLOG_DEBUG_STR(logger, velMinus << " " << vel << " " << velPlus);
        specIncr = fabs(velPlus.getValue() - velMinus.getValue()) / 2.;
    }
    else {
        // can't do velocity conversion, so just use the WCS spectral units
        specIncr = fabs(spcoo.increment()[0]);
    }
//    ASKAPLOG_DEBUG_STR(logger, "Channel " << z << " had spectral increment " << specIncr);
    return specIncr;
}

double MomentMapExtractor::getSpecVal(int z)
{
    int spcCoNum = itsInputCoords.findCoordinate(casa::Coordinate::SPECTRAL);
    casa::SpectralCoordinate spcoo(itsInputCoords.spectralCoordinate(spcCoNum));
    double specval;
    if (spcoo.restFrequency() > 0.) {
        casa::Quantum<Double> vel;
        ASKAPASSERT(spcoo.pixelToVelocity(vel, double(z)));
        specval = vel.getValue();
    }
    else {
        ASKAPASSERT(spcoo.toWorld(specval, double(z)));
    }
    return specval;
}

void MomentMapExtractor::getMom0(const casa::Array<Float> &subarray, const casa::LogicalArray &submask)
{

    ASKAPLOG_INFO_STR(logger, "Extracting moment-0 map");
    itsMom0map = casa::Array<Float>(this->arrayShape(), 0.0f);
    casa::LogicalArray basemask(this->arrayShape(), true);
    if (itsInputIsMasked) {
        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        casa::LogicalArray mskslice(ia->readMask(itsInputCube)(itsSlicer));
        casa::LogicalArray
        mskTmp = greaterThanZero(partialNTrue(mskslice, casa::IPosition(1, itsSpcAxis)));
        basemask = mskTmp.reform(this->arrayShape());
    }
    itsMom0mask = casa::LogicalArray(this->arrayShape(), false);

    // To get the mask to be applied in FITS images, we divide through
    // at the end by this array. Valid pixels have value of 1. Masked
    // pixels have value of 0. That way the pixels that should be
    // masked are converted to nans.
    casa::Array<float> maskScaler(this->arrayShape(), 0.f);

    casa::IPosition outloc(4, 0), inloc(4, 0);
    casa::IPosition start = itsSlicer.start();

    if (itsFlagUseDetection) {
        std::vector<PixelInfo::Voxel> voxlist = itsSource->getPixelSet();
        std::vector<PixelInfo::Voxel>::iterator vox;
        for (vox = voxlist.begin(); vox != voxlist.end(); vox++) {
            int x = vox->getX() - start(itsLngAxis) + itsSource->getXOffset();
            int y = vox->getY() - start(itsLatAxis) + itsSource->getYOffset();
            int zin = vox->getZ() - start(itsSpcAxis) + itsSource->getZOffset();
            int zfull = vox->getZ() + itsSource->getZOffset();
            outloc(itsLngAxis) = inloc(itsLngAxis) = x;
            outloc(itsLatAxis) = inloc(itsLatAxis) = y;
            inloc(itsSpcAxis) = zin;
            if (submask(inloc)) {
                itsMom0map(outloc) = itsMom0map(outloc) + subarray(inloc) * getSpectralIncrement(zfull);
            }
            itsMom0mask(outloc) = itsMom0mask(outloc) || submask(inloc);
            maskScaler(outloc) = 1.;
        }
    }
    else {
        // just sum each spectrum over the slicer's range.
        casa::IPosition outBLC(4, 0), outTRC(itsMom0map.shape() - 1);
        // Make an integer version of the mask to do partial maths on
        casa::Array<Int> submaskint(submask.shape(), 0);
        casa::Array<Float> submaskflt(submask.shape(), 0.f);
        casa::Array<Bool>::const_iterator iterM = submask.begin();
        casa::Array<Int>::iterator iterI = submaskint.begin();
        casa::Array<Float>::iterator iterF = submaskflt.begin();
        while (iterM != submask.end()) {
            if (*iterM) {
                *iterI = 1;
                *iterF = 1.;
            }
            iterM++;
            iterI++;
        }
        // remove any masked points from the subarray by setting them to zero
        casa::Array<Float> subarrayMasked = subarray * submaskflt;
        casa::Array<Float> sumarray = partialSums(subarrayMasked, casa::IPosition(1, itsSpcAxis));
        itsMom0map(outBLC, outTRC) = sumarray.reform(itsMom0map(outBLC, outTRC).shape()) * getSpectralIncrement();
        // mask anything that didn't have any good pixels in the box
        casa::LogicalArray summask = (partialSums(submaskint, casa::IPosition(1, itsSpcAxis)) > 0);
        itsMom0mask(outBLC, outTRC) = summask.reform(itsMom0map(outBLC, outTRC).shape());
    }
    itsMom0mask = itsMom0mask && basemask;

    itsMom0map /= maskScaler;

}

void MomentMapExtractor::getMom1(const casa::Array<Float> &subarray, const casa::LogicalArray &submask)
{
    ASKAPLOG_INFO_STR(logger, "Extracting moment-1 map");
    itsMom1map = casa::Array<Float>(this->arrayShape(), 0.0f);
    casa::LogicalArray basemask(this->arrayShape(), true);
    if (itsInputIsMasked) {
        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        casa::LogicalArray mskslice(ia->readMask(itsInputCube)(itsSlicer));
        casa::LogicalArray
        mskTmp = greaterThanZero(partialNTrue(mskslice, casa::IPosition(1, itsSpcAxis)));
        basemask = mskTmp.reform(this->arrayShape());
    }
    itsMom1mask = casa::LogicalArray(this->arrayShape(), false);

    casa::IPosition start = itsSlicer.start();

    if (itsMom0map.size() == 0) this->getMom0(subarray, submask);
    casa::Array<Float> sumNuS(itsMom1map.shape(), 0.0f);
    casa::Array<Float> sumS = itsMom0map / this->getSpectralIncrement();
    if (itsFlagUseDetection) {
        casa::IPosition outloc(4, 0), inloc(4, 0);
        std::vector<PixelInfo::Voxel> voxlist = itsSource->getPixelSet();
        std::vector<PixelInfo::Voxel>::iterator vox;
        for (vox = voxlist.begin(); vox != voxlist.end(); vox++) {
            int x = vox->getX() - start(itsLngAxis) + itsSource->getXOffset();
            int y = vox->getY() - start(itsLatAxis) + itsSource->getYOffset();
            int zin = vox->getZ() - start(itsSpcAxis) + itsSource->getZOffset();
            int zfull = vox->getZ() + itsSource->getZOffset();
            outloc(itsLngAxis) = inloc(itsLngAxis) = x;
            outloc(itsLatAxis) = inloc(itsLatAxis) = y;
            inloc(itsSpcAxis) = zin;
            if (submask(inloc)) {
                sumNuS(outloc) = sumNuS(outloc) +
                                 subarray(inloc) * this->getSpecVal(zfull) * getSpectralIncrement(zfull);
            }
            itsMom1mask(outloc) = itsMom1mask(outloc) || submask(inloc);
        }
    }
    else {
        // just sum each spectrum over the slicer's range.
        casa::IPosition outBLC(itsMom1map.ndim(), 0), outTRC(itsMom1map.shape() - 1);
        casa::Array<Float> nuArray(subarray.shape(), 0.f);
        for (int z = 0; z < subarray.shape()(itsSpcAxis); z++) {
            casa::IPosition blc(subarray.ndim(), 0), trc = subarray.shape() - 1;
            blc(itsSpcAxis) = trc(itsSpcAxis) = z;
            nuArray(blc, trc) = this->getSpecVal(z + start(itsSpcAxis));
        }
        // Make an integer version of the mask to do partial maths on
        casa::Array<Int> submaskint(submask.shape(), 0);
        casa::Array<Float> submaskflt(submask.shape(), 0.f);
        casa::Array<Bool>::const_iterator iterM = submask.begin();
        casa::Array<Int>::iterator iterI = submaskint.begin();
        casa::Array<Float>::iterator iterF = submaskflt.begin();
        while (iterM != submask.end()) {
            if (*iterM) {
                *iterI = 1;
                *iterF = 1.;
            }
            iterM++;
            iterI++;
        }
        // remove any masked points from the subarray by setting them to zero
        casa::Array<Float> subarrayMasked = subarray * submaskflt;
        casa::Array<Float> nuSubarray = nuArray * subarrayMasked;
        casa::Array<Float> sumarray = partialSums(nuSubarray, casa::IPosition(1, itsSpcAxis));
        sumNuS(outBLC, outTRC) = sumarray.reform(sumNuS(outBLC, outTRC).shape()) * this->getSpectralIncrement();
        // mask anything that didn't have any good pixels in the box
        casa::LogicalArray summask = (partialSums(submaskint, casa::IPosition(1, itsSpcAxis)) > 0);
        itsMom1mask(outBLC, outTRC) = summask.reform(itsMom1map(outBLC, outTRC).shape());
    }

    const float zero = 0.f;
    itsMom1mask = itsMom1mask && basemask;
    itsMom1mask = itsMom1mask && (itsMom0map > zero);

    itsMom1map = (sumNuS / itsMom0map);

}

void MomentMapExtractor::getMom2(const casa::Array<Float> &subarray, const casa::LogicalArray &submask)
{
    ASKAPLOG_INFO_STR(logger, "Extracting moment-2 map");
    itsMom2map = casa::Array<Float>(this->arrayShape(), 0.0f);
    casa::LogicalArray basemask(this->arrayShape(), true);
    if (itsInputIsMasked) {
        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        casa::LogicalArray mskslice(ia->readMask(itsInputCube)(itsSlicer));
        casa::LogicalArray
        mskTmp = greaterThanZero(partialNTrue(mskslice, casa::IPosition(1, itsSpcAxis)));
        basemask = mskTmp.reform(this->arrayShape());
    }
    itsMom2mask = casa::LogicalArray(this->arrayShape(), false);
    casa::IPosition start = itsSlicer.start();

    if (itsMom1map.size() == 0) this->getMom1(subarray, submask);
    casa::Array<Float> sumNu2S(itsMom2map.shape(), 0.0f);
    casa::Array<Float> sumS = itsMom0map / this->getSpectralIncrement();
    if (itsFlagUseDetection) {
        casa::IPosition outloc(4, 0), inloc(4, 0);
        std::vector<PixelInfo::Voxel> voxlist = itsSource->getPixelSet();
        std::vector<PixelInfo::Voxel>::iterator vox;
        for (vox = voxlist.begin(); vox != voxlist.end(); vox++) {
            int x = vox->getX() - start(itsLngAxis) + itsSource->getXOffset();
            int y = vox->getY() - start(itsLatAxis) + itsSource->getYOffset();
            int zin = vox->getZ() - start(itsSpcAxis) + itsSource->getZOffset();
            int zfull = vox->getZ() + itsSource->getZOffset();
            outloc(itsLngAxis) = inloc(itsLngAxis) = x;
            outloc(itsLatAxis) = inloc(itsLatAxis) = y;
            inloc(itsSpcAxis) = zin;
            if (submask(inloc)) {
                sumNu2S(outloc) = sumNu2S(outloc) +
                                  subarray(inloc) *
                                  (this->getSpecVal(zfull) - itsMom1map(outloc)) *
                                  (this->getSpecVal(zfull) - itsMom1map(outloc)) *
                                  getSpectralIncrement(zfull);
            }
            itsMom2mask(outloc) = itsMom2mask(outloc) || submask(inloc);
        }
    }
    else {
        // just sum each spectrum over the slicer's range.
        casa::IPosition outBLC(itsMom2map.ndim(), 0), outTRC(itsMom2map.shape() - 1);
        casa::IPosition shapeIn(subarray.shape());
        casa::IPosition shapeMap(shapeIn); shapeMap(itsSpcAxis) = 1;
        casa::Array<Float> nu2Array(shapeIn, 0.f);
        casa::Array<Float> meanNu(itsMom1map.reform(shapeMap));
        for (int z = 0; z < subarray.shape()(itsSpcAxis); z++) {
            casa::IPosition blc(subarray.ndim(), 0), trc = subarray.shape() - 1;
            blc(itsSpcAxis) = trc(itsSpcAxis) = z;
            nu2Array(blc, trc) = this->getSpecVal(z + start(itsSpcAxis));
            nu2Array(blc, trc) = (nu2Array(blc, trc) - meanNu);
        }
        // Make an integer version of the mask to do partial maths on
        casa::Array<Int> submaskint(submask.shape(), 0);
        casa::Array<Float> submaskflt(submask.shape(), 0.f);
        casa::Array<Bool>::const_iterator iterM = submask.begin();
        casa::Array<Int>::iterator iterI = submaskint.begin();
        casa::Array<Float>::iterator iterF = submaskflt.begin();
        while (iterM != submask.end()) {
            if (*iterM) {
                *iterI = 1;
                *iterF = 1.;
            }
            iterM++;
            iterI++;
        }
        // remove any masked points from the subarray by setting them to zero
        casa::Array<Float> subarrayMasked = subarray * submaskflt;
        casa::Array<Float> nu2Subarray = nu2Array * nu2Array * subarrayMasked;
        casa::Array<Float> sumarray = partialSums(nu2Subarray, casa::IPosition(1, itsSpcAxis));
        sumNu2S(outBLC, outTRC) = sumarray.reform(sumNu2S(outBLC, outTRC).shape()) * this->getSpectralIncrement();
        // mask anything that didn't have any good pixels in the box
        casa::LogicalArray summask = (partialSums(submaskint, casa::IPosition(1, itsSpcAxis)) > 0);
        itsMom2mask(outBLC, outTRC) = summask.reform(itsMom2map(outBLC, outTRC).shape());
    }

    itsMom2map = (sumNu2S / itsMom0map);

    const float zero = 0.f;
    itsMom2mask = itsMom2mask && basemask;
    itsMom2mask = itsMom2mask && (itsMom0map > zero);
    itsMom2mask = itsMom2mask && (itsMom2map > zero);

    itsMom2map = sqrt(itsMom2map);


}


}

}
