/// @file
///
/// Base class for handling extraction of image data corresponding to a source
///
/// @copyright (c) 2017 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/SourceDataExtractor.h>
#include <askap_analysis.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <string>
#include <askap/analysis/sourcefitting/RadioSource.h>
#include <askap/analysisutilities/casainterface/CasaInterface.h>
#include <askap/imageaccess/ImageAccessFactory.h>
#include <askap/analysis/catalogues/CasdaComponent.h>
#include <askap/analysis/catalogues/CasdaIsland.h>

#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/BasicSL/String.h>
#include <casacore/lattices/Lattices/LatticeBase.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/casa/Quanta/Unit.h>
#include <Common/ParameterSet.h>
#include <casacore/measures/Measures/Stokes.h>
#include <boost/shared_ptr.hpp>

#include <askap/scimath/utils/PolConverter.h>

ASKAP_LOGGER(logger, ".sourcedataextractor");

namespace askap {

namespace analysis {

/// removes ambiguity
using casacore::operator<<;

SourceDataExtractor::SourceDataExtractor(const LOFAR::ParameterSet& parset):
    itsParset(parset),
    itsSource(0),
    itsComponent(0),
    itsSourceInBounds(false),
    itsObjID(""),
    itsObjectName(""),
    itsInputImageOK(false),
    itsReadyToExtract(false)
{
    itsInputCube = ""; // start off with this blank. Needs to be
    // set before calling openInput()
    itsInputCubeList = parset.getStringVector("spectralCube",
                       std::vector<std::string>(0));

    // Add imagetype=fits if not given in parset
    // This is for backwards-compatibility
    if (!itsParset.isDefined("imagetype")) {
        itsParset.add("imagetype", "fits");
    }

    // Take the following from SynthesisParamsHelper.cc in Synthesis
    // there could be many ways to define stokes, e.g. ["XX YY"] or
    // ["XX","YY"] or "XX,YY" to allow some flexibility we have to
    // concatenate all elements first and then allow the parser from
    // PolConverter to take care of extracting the products.
    const std::vector<std::string>
    stokesVec = parset.getStringVector("polarisation",
                                       std::vector<std::string>(1, "I"));
    std::string stokesStr;
    for (size_t i = 0; i < stokesVec.size(); ++i) {
        stokesStr += stokesVec[i];
    }
    itsStokesList = scimath::PolConverter::fromString(stokesStr);

    this->verifyInputs();

    openInput();
    itsOutputUnits = itsInputUnits;

}

SourceDataExtractor::~SourceDataExtractor()
{
}

casa::Vector<Quantum<Double> > SourceDataExtractor::inputBeam()
{
    return itsInputBeam;
}


casa::IPosition SourceDataExtractor::getShape(std::string image)
{
    itsInputCube = image;
    openInput();
    return itsInputShape;
}
//---------------
template <class T> double SourceDataExtractor::getRA(T &object)
{
    return object.ra();
}
template double SourceDataExtractor::getRA<CasdaComponent>(CasdaComponent &object);
template double SourceDataExtractor::getRA<CasdaIsland>(CasdaIsland &object);
template <> double SourceDataExtractor::getRA<RadioSource>(RadioSource &object)
{
    return object.getRA();
}
//---------------
template <class T> double SourceDataExtractor::getDec(T &object)
{
    return object.dec();
}
template double SourceDataExtractor::getDec<CasdaComponent>(CasdaComponent &object);
template double SourceDataExtractor::getDec<CasdaIsland>(CasdaIsland &object);
template <> double SourceDataExtractor::getDec<RadioSource>(RadioSource &object)
{
    return object.getDec();
}
//---------------
template <class T> std::string SourceDataExtractor::getID(T &obj)
{
    int ID = obj.getID();
    std::stringstream ss;
    ss << ID;
    return ss.str();
}
template std::string SourceDataExtractor::getID<RadioSource>(RadioSource &obj);
template <> std::string SourceDataExtractor::getID<CasdaComponent>(CasdaComponent &obj)
{
    return obj.componentID();
}
template <> std::string SourceDataExtractor::getID<CasdaIsland>(CasdaIsland &obj)
{
    return obj.id();
}
//---------------
template <class T>
void SourceDataExtractor::setSourceLoc(T* src)
{
    itsSourceID = getID(*src);

    std::stringstream ss;
    ss << itsOutputFilenameBase << "_" << itsObjID;
    itsOutputFilename = ss.str();
    ASKAPLOG_DEBUG_STR(logger, "SourceDataExtractor for source " << itsOutputFilename);
    double ra = getRA(*src);
    double dec = getDec(*src);
    ASKAPLOG_DEBUG_STR(logger, "RA = " << ra << ", Dec = " << dec);
    casa::DirectionCoordinate dc = itsInputCoords.directionCoordinate();
    casa::Vector<casa::Double> pix(2, 0.);
    casa::Vector<casa::Double> wld(2);
    wld[0] = ra * M_PI / 180.;
    wld[1] = dec * M_PI / 180.;
    dc.toPixel(pix, wld);
    ASKAPLOG_DEBUG_STR(logger, "Converting to pixel coords from world=" << wld << " to get pix=" << pix);
    // ASKAPLOG_DEBUG_STR(logger, "Direction coordinate ref: " << dc.referenceValue()*180./M_PI << " at " << dc.referencePixel());
    // ASKAPLOG_DEBUG_STR(logger, "Direction coordinate inc: " << dc.increment()*180./M_PI);
    itsXloc = pix[0];
    itsYloc = pix[1];

    int x(itsXloc);
    int y(itsYloc);
    ASKAPLOG_DEBUG_STR(logger, "Comparing pixel loc [" << x << "," << y << "] with input cube shape " << itsInputShape);
    itsSourceInBounds = ((x >= 0) && (x < itsInputShape[0])) && ((y >= 0) && (y < itsInputShape[1]));

}
template void SourceDataExtractor::setSourceLoc<CasdaComponent>(CasdaComponent* src);
template void SourceDataExtractor::setSourceLoc<RadioSource>(RadioSource* src);

bool SourceDataExtractor::setSource(RadioSource* src)
{

    itsSource = src;

    if (itsSource) {

        itsObjectName = src->getName();
        itsObjID      = src->getID();
        setSourceLoc(src);
        if (!itsSourceInBounds) {
            ASKAPLOG_DEBUG_STR(logger, "Source " << itsObjID << " (" << itsObjectName << ") is not in the bounds of the input cube and will not be extracted.");
        }
    }
    return itsSourceInBounds;

}
bool SourceDataExtractor::setSource(CasdaComponent* src)
{
    itsComponent = src;

    if (itsComponent) {

        itsObjectName = src->name();
        itsObjID      = src->componentID();
        setSourceLoc(src);
        if (!itsSourceInBounds) {
            ASKAPLOG_DEBUG_STR(logger, "Component " << itsObjID << " (" << itsObjectName << ") is not in the bounds of the input cube and will not be extracted.");
        }

    }
    return itsSourceInBounds;
}
//---------------



bool SourceDataExtractor::checkPol(std::string image,
                                   casa::Stokes::StokesTypes stokes)
{

    itsInputCube = image;
    const casacore::Vector<casa::Stokes::StokesTypes> stokesvec(1, stokes);
    std::string polstring = scimath::PolConverter::toString(stokesvec)[0];

    bool haveMatch = false;
    if (this->openInput()) {
        int stokeCooNum = itsInputCoords.polarizationCoordinateNumber();
        if (stokeCooNum > -1) {

            const casa::StokesCoordinate
            stokeCoo = itsInputCoords.stokesCoordinate(stokeCooNum);
            if (stokeCooNum == -1 || itsStkAxis == -1) {
                ASKAPCHECK(polstring == "I", "Extraction: Input cube " << image <<
                           " has no polarisation axis, but you requested " << polstring);
            }
            else {
                int nstoke = itsInputShape[itsStkAxis];
                for (int i = 0; i < nstoke && !haveMatch; i++) {
                    haveMatch = haveMatch || (stokeCoo.stokes()[i] == stokes);
                }
            }
        }
        else {
            ASKAPLOG_WARN_STR(logger, "Input cube has no Stokes axis - assuming it is Stokes I");
            // No Stokes axis - assume it is Stokes I
            haveMatch = (stokes == casa::Stokes::I);
        }
    }
    else ASKAPLOG_ERROR_STR(logger, "Could not open image");
    return haveMatch;
}

void SourceDataExtractor::verifyInputs()
{
    std::vector<std::string>::iterator im;
    std::vector<std::string> pollist = scimath::PolConverter::toString(itsStokesList);
    casa::Stokes stokes;
    ASKAPCHECK(itsInputCubeList.size() > 0,
               "Extraction: You have not provided a spectralCube input");
    ASKAPCHECK(itsStokesList.size() > 0,
               "Extraction: You have not provided a list of Stokes parameters " <<
               "(input parameter \"polarisation\")");

    if (itsInputCubeList.size() > 1) { // multiple input cubes provided

        // check they are all the same shape
        casa::IPosition refShape = this->getShape(itsInputCubeList[0]);
        for (size_t i = 1; i < itsInputCubeList.size(); i++) {
            ASKAPCHECK(refShape == this->getShape(itsInputCubeList[i]),
                       "Extraction: shapes of " << itsInputCubeList[0] <<
                       " and " << itsInputCubeList[i] << " do not match");
        }

        for (im = itsInputCubeList.begin(); im < itsInputCubeList.end(); im++) {
            for (size_t i = 0; i < itsStokesList.size(); i++) {
                if (checkPol(*im, itsStokesList[i])) {
                    ASKAPLOG_DEBUG_STR(logger, "Stokes " << stokes.name(itsStokesList[i]) << " has image " << *im);
                    itsCubeStokesMap.insert(
                        std::pair<casa::Stokes::StokesTypes, std::string>(itsStokesList[i], *im));
                }
            }
        }

    }
    else {
        // only have a single input cube

        if (itsInputCubeList[0].find("%p") != std::string::npos) {
            // the filename has a "%p" string, meaning
            // polarisation substitution is possible
            for (size_t i = 0; i < itsStokesList.size(); i++) {
                casa::String stokesname(stokes.name(itsStokesList[i]));
                stokesname.downcase();
                std::string input = itsInputCubeList[0];
                ASKAPLOG_DEBUG_STR(logger, "Input cube name: replacing \"%p\" with " <<
                                   stokesname.c_str() << " in " << input);
                input.replace(input.find("%p"), 2, stokesname.c_str());
                if (checkPol(input, itsStokesList[i])) {
                    ASKAPLOG_DEBUG_STR(logger, "Stokes " << stokes.name(itsStokesList[i]) << " has image " << input);
                    itsCubeStokesMap.insert(
                        std::pair<casa::Stokes::StokesTypes, std::string>(itsStokesList[i], input));
                }
            }
        }
        else {
            // We aren't using the %p wildcard - does its polarisation match one of the ones provided?
            bool hasMatch = false;
            for (size_t i = 0; i < itsStokesList.size(); i++) {
                hasMatch = checkPol(itsInputCubeList[0], itsStokesList[i]);
                if (hasMatch) {
                    ASKAPLOG_DEBUG_STR(logger, "Stokes " << stokes.name(itsStokesList[i]) << " has image " << itsInputCubeList[0]);
                    itsCubeStokesMap.insert(
                        std::pair<casa::Stokes::StokesTypes, std::string>(itsStokesList[i],
                                itsInputCubeList[0]));
                }
            }
            ASKAPCHECK(hasMatch,
                       "Image " << itsInputCubeList[0] << " does not match any requested Stokes");
        }
    }
    ASKAPLOG_DEBUG_STR(logger, "CubeStokesMap: " << itsCubeStokesMap);

}


void SourceDataExtractor::writeBeam(std::string &filename)
{
    if (itsInputBeam.size() > 0) {
        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        ia->setBeamInfo(filename,
                        itsInputBeam[0].getValue("rad"),
                        itsInputBeam[1].getValue("rad"),
                        itsInputBeam[2].getValue("rad"));
    }
    else {
        ASKAPLOG_WARN_STR(logger,
                          "Input cube has no restoring beam, so cannot write to output image.");
    }
}

casa::Unit SourceDataExtractor::bunit()
{
    return itsInputUnits;
}


bool SourceDataExtractor::openInput()
{
    itsInputImageOK = (itsInputCube != "");
    if (!itsInputImageOK) {
        ASKAPLOG_ERROR_STR(logger, "Image name is empty - cannot open!");
    }
    else {
        boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
        itsInputCoords = ia->coordSys(itsInputCube);
        itsLngAxis = itsInputCoords.directionAxesNumbers()[0];
        itsLatAxis = itsInputCoords.directionAxesNumbers()[1];
        itsSpcAxis = itsInputCoords.spectralAxisNumber();
        itsStkAxis = itsInputCoords.polarizationAxisNumber();
        itsInputUnits = ia->getUnits(itsInputCube);
        itsInputShape = ia->shape(itsInputCube);
        itsInputBeam = ia->beamInfo(itsInputCube);
        itsInputIsMasked = ia->isMasked(itsInputCube);
    }
    return itsInputImageOK;
}

void SourceDataExtractor::setObjectIDs(const std::string &objid, const std::string &objectname)
{
    itsObjID = objid;
    itsObjectName = objectname;
}


void SourceDataExtractor::updateHeaders(const std::string &filename)
{

    boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
    ASKAPLOG_INFO_STR(logger, "SourceDataExtractor::updateHeaders :: itsParset = " << itsParset);

    // set the object ID and object name keywords
    if (itsObjID != "") {
        ASKAPLOG_DEBUG_STR(logger, "Writing OBJID=" << itsObjID << " to header");
        ia->setMetadataKeyword(filename, "OBJID", itsObjID, "Object ID");
    }
    if (itsObjectName != "") {
        ASKAPLOG_DEBUG_STR(logger, "Writing OBJECT=" << itsObjectName << " to header");
        ia->setMetadataKeyword(filename, "OBJECT", itsObjectName, "IAU-format Object Name");
    }

    ASKAPLOG_DEBUG_STR(logger, "Copying header keywords from " << itsInputCube << " to " << filename);

    std::pair<std::string, std::string> valueAndComment;
    // set the other required keywords by copying from input file
    valueAndComment = ia->getMetadataKeyword(itsInputCube, "DATE-OBS");
    if (valueAndComment.first != "") {
        ia->setMetadataKeyword(filename, "DATE-OBS", valueAndComment.first, valueAndComment.second);
    }
    valueAndComment = ia->getMetadataKeyword(itsInputCube, "DURATION");
    if (valueAndComment.first != "") {
        ia->setMetadataKeyword(filename, "DURATION", valueAndComment.first, valueAndComment.second);
    }
    valueAndComment = ia->getMetadataKeyword(itsInputCube, "PROJECT");
    if (valueAndComment.first != "") {
        ia->setMetadataKeyword(filename, "PROJECT", valueAndComment.first, valueAndComment.second);
    }
    valueAndComment = ia->getMetadataKeyword(itsInputCube, "SBID");
    if (valueAndComment.first != "") {
        ia->setMetadataKeyword(filename, "SBID", valueAndComment.first, valueAndComment.second);
    }

    ia->addHistory(filename, itsParset.getStringVector("imageHistory", {}, false));

}

}

}
