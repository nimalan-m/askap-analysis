/// @file
///
/// Class to handle extraction of a summed spectrum corresponding to a source.
///
/// @copyright (c) 2011 CSIRO
/// Australia Telescope National Facility (ATNF)
/// Commonwealth Scientific and Industrial Research Organisation (CSIRO)
/// PO Box 76, Epping NSW 1710, Australia
/// atnf-enquiries@csiro.au
///
/// This file is part of the ASKAP software distribution.
///
/// The ASKAP software distribution is free software: you can redistribute it
/// and/or modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of the License,
/// or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful,
/// but WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/// GNU General Public License for more details.
///
/// You should have received a copy of the GNU General Public License
/// along with this program; if not, write to the Free Software
/// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
///
/// @author Matthew Whiting <Matthew.Whiting@csiro.au>
///
#include <askap/analysis/extraction/SourceSpectrumExtractor.h>
#include <askap_analysis.h>
#include <askap/analysis/extraction/SourceDataExtractor.h>
#include <askap/analysis/extraction/SpectralBoxExtractor.h>

#include <askap/askap/AskapLogging.h>
#include <askap/askap/AskapError.h>

#include <askap/analysis/sourcefitting/RadioSource.h>

#include <askap/imageaccess/ImageAccessFactory.h>
#include <askap/imageaccess/BeamLogger.h>

#include <duchamp/PixelMap/Object2D.hh>

#include <casacore/casa/Arrays/IPosition.h>
#include <casacore/casa/Arrays/Array.h>
#include <casacore/casa/Arrays/MaskedArray.h>
#include <casacore/casa/Arrays/Slicer.h>
#include <casacore/casa/Arrays/Vector.h>
#include <casacore/images/Images/SubImage.h>
#include <casacore/coordinates/Coordinates/CoordinateSystem.h>
#include <casacore/coordinates/Coordinates/DirectionCoordinate.h>
#include <casacore/measures/Measures/Stokes.h>

#include <Common/ParameterSet.h>

using namespace askap::analysis::sourcefitting;

ASKAP_LOGGER(logger, ".sourcespectrumextractor");

namespace askap {

namespace analysis {

SourceSpectrumExtractor::SourceSpectrumExtractor(const LOFAR::ParameterSet& parset):
    SpectralBoxExtractor(parset)
{

    itsFlagUseDetection = parset.getBool("useDetectedPixels", false);
    if (itsFlagUseDetection) {
        itsBoxWidth = -1;
        if (parset.isDefined("spectralBoxWidth")) {
            ASKAPLOG_WARN_STR(logger, "useDetectedPixels option selected, " <<
                              "so setting spectralBoxWidth=-1");
        }
    }

    itsFlagDoScale = parset.getBool("scaleSpectraByBeam", true);
    itsBeamLog = parset.getString("beamLog", "");

    for (size_t stokes = 0; stokes < itsStokesList.size(); stokes++) {
        itsCurrentStokes = itsStokesList[stokes];
        itsInputCube = itsCubeStokesMap[itsCurrentStokes];
        this->initialiseArray();
    }

}


void SourceSpectrumExtractor::setBeamScale()
{
    for (size_t stokes = 0; stokes < itsStokesList.size(); stokes++) {

        // get either the matching image for the current stokes value,
        // or the first&only in the input list
        itsCurrentStokes = itsStokesList[stokes];
        itsInputCube = itsCubeStokesMap[itsCurrentStokes];

        itsBeamScaleFactor[itsCurrentStokes] = std::vector<float>();
        ASKAPLOG_DEBUG_STR(logger, "About to find beam scale for Stokes " << itsCurrentStokes << " and image " << itsInputCube);

        if (itsFlagDoScale) {

            if (this->openInput()) {

                // Change the output units to remove the "/beam" extension
                std::string inunit = itsInputUnits;
                if (inunit.substr(inunit.size() - 5, inunit.size()) == "/beam") {
                    itsOutputUnits.setName(inunit.substr(0, inunit.size() - 5));
                }

                std::map<unsigned int, casa::Vector<Quantum<Double> > > beamvec;

                ASKAPLOG_DEBUG_STR(logger, "Setting beam scaling factor. BeamLog=" <<
                                   itsBeamLog << ", image beam = " << itsInputBeam);

                if (itsBeamLog == "") {
                    if (itsInputBeam.size() == 0) {
                        ASKAPLOG_WARN_STR(logger, "Input image \"" << itsInputCube <<
                                          "\" has no beam information. Not scaling spectra by beam");
                        itsBeamScaleFactor[itsCurrentStokes].push_back(1.);
                    }
                    else {
                        beamvec[0] = itsInputBeam;
                        ASKAPLOG_DEBUG_STR(logger, "Beam for input cube = " << itsInputBeam);
                    }
                }
                else {
                    std::string beamlogfile = itsBeamLog;
                    if (itsBeamLog.find("%p") != std::string::npos) {
                        // the beam log has a "%p" string, meaning
                        // polarisation substitution is possible
                        casa::Stokes stokes;
                        casa::String stokesname(stokes.name(itsCurrentStokes));
                        stokesname.downcase();
                        ASKAPLOG_DEBUG_STR(logger, "Input beam log: replacing \"%p\" with " <<
                                           stokesname.c_str() << " in " << beamlogfile);
                        beamlogfile.replace(beamlogfile.find("%p"), 2, stokesname.c_str());
                    }
                    accessors::BeamLogger beamlog(beamlogfile);
                    beamlog.read();
                    beamvec = beamlog.beamlist();

                    if (int(beamvec.size()) != itsInputShape(itsSpcAxis)) {
                        ASKAPLOG_ERROR_STR(logger, "Beam log " << itsBeamLog <<
                                           " has " << beamvec.size() <<
                                           " entries - was expecting " <<
                                           itsInputShape(itsSpcAxis));
                        beamvec[0] = itsInputBeam;
                    }
                }

                if (beamvec.size() > 0) {

                    for (size_t i = 0; i < beamvec.size(); i++) {

                        int dirCoNum = itsInputCoords.findCoordinate(casa::Coordinate::DIRECTION);
                        casa::DirectionCoordinate
                        dirCoo = itsInputCoords.directionCoordinate(dirCoNum);
                        double fwhmMajPix = beamvec[i][0].getValue(dirCoo.worldAxisUnits()[0]) /
                                            fabs(dirCoo.increment()[0]);
                        double fwhmMinPix = beamvec[i][1].getValue(dirCoo.worldAxisUnits()[1]) /
                                            fabs(dirCoo.increment()[1]);

                        if (itsFlagUseDetection) {
                            double bpaDeg = beamvec[i][2].getValue("deg");
                            duchamp::DuchampBeam beam(fwhmMajPix, fwhmMinPix, bpaDeg);
                            itsBeamScaleFactor[itsCurrentStokes].push_back(beam.area());
                            if (itsBeamLog == "") {
                                ASKAPLOG_DEBUG_STR(logger, "Stokes " << itsCurrentStokes << " has beam scale factor = " <<
                                                   itsBeamScaleFactor[itsCurrentStokes] << " using beam of " <<
                                                   fwhmMajPix << "x" << fwhmMinPix);
                            }
                        }
                        else {

                            double costheta = cos(beamvec[i][2].getValue("rad"));
                            double sintheta = sin(beamvec[i][2].getValue("rad"));

                            double majSDsq = fwhmMajPix * fwhmMajPix / 8. / M_LN2;
                            double minSDsq = fwhmMinPix * fwhmMinPix / 8. / M_LN2;

                            int hw = (itsBoxWidth - 1) / 2;
                            double scaleFactor = 0.;
                            for (int y = -hw; y <= hw; y++) {
                                for (int x = -hw; x <= hw; x++) {
                                    double u = x * costheta + y * sintheta;
                                    double v = x * sintheta - y * costheta;
                                    scaleFactor += exp(-0.5 * (u * u / majSDsq + v * v / minSDsq));
                                }
                            }
                            itsBeamScaleFactor[itsCurrentStokes].push_back(scaleFactor);

                            if (itsBeamLog == "") {
                                ASKAPLOG_DEBUG_STR(logger, "Stokes " << itsCurrentStokes << " has beam scale factor = " <<
                                                   itsBeamScaleFactor[itsCurrentStokes]);
                            }

                        }
                    }

                }

                ASKAPLOG_DEBUG_STR(logger,
                                   "Defined the beam scale factor vector of size " <<
                                   itsBeamScaleFactor[itsCurrentStokes].size());

            }
            else {
                ASKAPLOG_ERROR_STR(logger, "Could not open image \"" << itsInputCube << "\".");
            }
        }
    }
}

void SourceSpectrumExtractor::extract()
{

    this->setBeamScale();

    for (size_t stokes = 0; stokes < itsStokesList.size(); stokes++) {

        // get either the matching image for the current stokes value,
        // or the first&only in the input list
        itsCurrentStokes = itsStokesList[stokes];
        itsInputCube = itsCubeStokesMap[itsCurrentStokes];
        ASKAPLOG_INFO_STR(logger, "Extracting spectrum for Stokes " << itsCurrentStokes
                          << " from image \"" << itsInputCube << "\".");
        itsReadyToExtract = false;
        this->defineSlicer();
        if (this->openInput() && itsReadyToExtract) {
            casa::Stokes stk;
            ASKAPLOG_INFO_STR(logger, "Extracting spectrum from " << itsInputCube <<
                              " with shape " << itsInputShape <<
                              " for source ID " << itsSourceID <<
                              " using slicer " << itsSlicer <<
                              " and Stokes " << stk.name(itsCurrentStokes));

            boost::shared_ptr<accessors::IImageAccess<casacore::Float> > ia = accessors::imageAccessFactory(itsParset);
            casa::Array<Float> subarray(ia->read(itsInputCube, itsSlicer.start(), itsSlicer.end()));
            casa::LogicalArray submask(ia->readMask(itsInputCube, itsSlicer.start(), itsSlicer.end()));
            // Make an integer version of the mask to do partial maths on
            casa::Array<Int> submaskint(submask.shape(), 0);
            casa::Array<Bool>::iterator iterM = submask.begin();
            casa::Array<Int>::iterator iterI = submaskint.begin();
            while (iterM != submask.end()) {
                if (*iterM) *iterI = 1;
                iterM++;
                iterI++;
            }

            casa::IPosition outBLC(itsArray.ndim(), 0), outTRC(itsArray.shape() - 1);
            if (itsStkAxis > -1) {
                // If there is a Stokes axis in the input file
                outBLC(itsStkAxis) = outTRC(itsStkAxis) = stokes;
            }

            if (!itsFlagUseDetection) {
                // sum the array over the box, then reshape to fit the output array
                casa::Array<Float> sumarray = partialSums(subarray, IPosition(2, 0, 1));
                itsArray(outBLC, outTRC) = sumarray.reform(itsArray(outBLC, outTRC).shape());

                // sum the mask values over the box
                // if one or more pixel is masked, the sum will end up less than the number of box pixels
                // use this as a test for masked-ness
                casa::Array<Int> summaskint = partialSums(submaskint, IPosition(2, 0, 1));
                int boxsize = submaskint.shape()[0] * submaskint.shape()[1];
                casa::Array<Int> reference(summaskint.shape(), boxsize);
                casa::LogicalArray summask = (summaskint == reference);
                itsMask(outBLC, outTRC) = summask.reform(itsArray(outBLC, outTRC).shape());
            }
            else {
                ASKAPASSERT(itsSource);
                ASKAPLOG_INFO_STR(logger,
                                  "Extracting integrated spectrum using " <<
                                  "all detected spatial pixels");

                PixelInfo::Object2D spatmap = itsSource->getSpatialMap();
                casa::IPosition blc(itsInputShape.size(), 0);
                casa::IPosition trc(itsInputShape.size(), 0);
                casa::IPosition inc(itsInputShape.size(), 1);

                trc(itsSpcAxis) = itsInputShape[itsSpcAxis] - 1;
                if (itsStkAxis > -1) {
                    casa::Stokes stk;
                    blc(itsStkAxis) = trc(itsStkAxis) =
                                          itsInputCoords.stokesPixelNumber(stk.name(itsCurrentStokes));
                }

                for (int x = itsSource->getXmin(); x <= itsSource->getXmax(); x++) {
                    for (int y = itsSource->getYmin(); y <= itsSource->getYmax(); y++) {
                        if (spatmap.isInObject(x, y)) {
                            blc(itsLngAxis) = trc(itsLngAxis) = x - itsSource->getXmin();
                            blc(itsLatAxis) = trc(itsLatAxis) = y - itsSource->getYmin();
                            casa::Array<Float> spec = subarray(blc, trc, inc);
                            casa::LogicalArray mask = submask(blc, trc, inc);
                            spec = spec.reform(itsArray(outBLC, outTRC).shape());
                            itsArray(outBLC, outTRC) = itsArray(outBLC, outTRC) + spec;
                            itsMask(outBLC, outTRC) = itsMask(outBLC, outTRC) * mask;
                        }
                    }
                }
            }
        }
        else {
            ASKAPLOG_ERROR_STR(logger, "Could not open image \"" << itsInputCube << "\".");
        }
    }


    if (itsFlagDoScale) {

        if (itsBeamScaleFactor[itsCurrentStokes].size() == 1) {
            itsArray /= itsBeamScaleFactor[itsCurrentStokes][0];
        }
        else {
            casa::IPosition start(itsArray.ndim(), 0);
            casa::IPosition end = itsArray.shape() - 1;
            start(itsLngAxis) = start(itsLatAxis) = 0;
            for (int z = 0; z < itsArray.shape()(itsSpcAxis); z++) {
                start(itsSpcAxis) = end(itsSpcAxis) = z;
                itsArray(start, end) = itsArray(start, end) / itsBeamScaleFactor[itsCurrentStokes][z];
            }
        }

    }

}


}
}
