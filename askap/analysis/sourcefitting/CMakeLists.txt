#
# askap/analysis/sourcefitting
#
add_sources_to_analysis(
CurvatureMapCreator.cc
FitResults.cc
Fitter.cc
FittingParameters.cc
RadioSource.cc
SubComponent.cc
SubThresholder.cc
)

install (FILES
CurvatureMapCreator.h
FitResults.h
Fitter.h
FittingParameters.h
RadioSource.h
SubComponent.h
SubThresholder.h	
DESTINATION include/askap/analysis/sourcefitting
)

