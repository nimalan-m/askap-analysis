#
# askap/simulations/makemodelslice
#
add_sources_to_analysis(
MakeAllModelSlicesApp.cc
MakeModelSliceApp.cc
SliceMaker.cc
)


install (FILES
MakeAllModelSlicesApp.h
MakeModelSliceApp.h
SliceMaker.h
 DESTINATION include/askap/simulations/makemodelslice
)

