// -----------------------------------------------------------------------
// CatalogueSpecification.cc: Define the specification of a catalogue,
//                            being a set of Columns
// -----------------------------------------------------------------------
// Copyright (C) 2006, Matthew Whiting, ATNF
//
// This program is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version.
//
// Duchamp is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General Public License
// along with Duchamp; if not, write to the Free Software Foundation,
// Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
//
// Correspondence concerning Duchamp may be directed to:
//    Internet email: Matthew.Whiting [at] atnf.csiro.au
//    Postal address: Dr. Matthew Whiting
//                    Australia Telescope National Facility, CSIRO
//                    PO Box 76
//                    Epping NSW 1710
//                    AUSTRALIA
// -----------------------------------------------------------------------
#include <duchamp/Outputs/CatalogueSpecification.hh>
#include <duchamp/duchamp.hh>
#include <duchamp/Outputs/columns.hh>
#include <duchamp/Detection/detection.hh>
#include <duchamp/fitsHeader.hh>
#include <vector>
#include <map>
#include <string>

namespace duchamp {

  namespace Catalogues {

    CatalogueSpecification::CatalogueSpecification(const CatalogueSpecification& other)
    {
      operator=(other);
    }

    CatalogueSpecification& CatalogueSpecification::operator= (const CatalogueSpecification& other)
    {
      if(this == &other) return *this;
      this->itsColumnList = other.itsColumnList;
      this->itsTypeMap = other.itsTypeMap;
      this->itsCommentString = other.itsCommentString;
      return *this;
    }

    void CatalogueSpecification::addColumn(Column col)
    {
      this->removeColumn(col.type());
      this->itsColumnList.push_back(col);
      // this->itsTypeMap[col.type()] = this->itsColumnList.size() - 1;
      this->setMap();
    }

    void CatalogueSpecification::addColumn(std::string type, std::string name, std::string units, int width, int prec, std::string ucd, std::string datatype, std::string colID, std::string extraInfo)
    {
        Column col(type,name,units,width,prec,ucd,datatype,colID,extraInfo);
        this->addColumn(col);
    }
      
      
      void CatalogueSpecification::setMap()
      {
	  for(size_t i=0;i<this->itsColumnList.size();i++)
	      this->itsTypeMap[this->itsColumnList[i].type()] = i;
      }

    void CatalogueSpecification::removeColumn(std::string type)
    {
	if( this->itsTypeMap.find(type) != this->itsTypeMap.end() ) // already in list
	    this->itsColumnList.erase( this->itsColumnList.begin()+this->itsTypeMap[type] );
	this->setMap();
    }

      bool CatalogueSpecification::hasColumn(std::string type)
      {
	  return this->itsTypeMap.find(type) != this->itsTypeMap.end();
      }

    void CatalogueSpecification::outputTableHeader(std::ostream &stream, Catalogues::DESTINATION tableType, bool flagWCS)
    {
      /// @details
      ///  Prints the header row for a table of detections. The columns
      ///  that are included depend on the value of tableType, according
      ///  to the Column::doCol() function. The format is a row of
      ///  dashes, a row with column names, a row with column units, and
      ///  another row of dashes.
      /// \param stream Where the output is written
      /// \param columns The vector list of Column objects
      /// \param tableType A string saying what format to use: one of
      /// "file", "log", "screen" or "votable" (although the latter
      /// shouldn't be used with this function).
      /// \param flagWCS A flag for use with Column::doCol(), specifying
      /// whether to use FINT or FTOT.

      stream << this->itsCommentString;
      for(size_t i=0;i<this->itsColumnList.size();i++)
	if(this->itsColumnList[i].doCol(tableType,flagWCS)) this->itsColumnList[i].printDash(stream);
      stream << "\n"<<this->itsCommentString;
      for(size_t i=0;i<this->itsColumnList.size();i++)
	if(this->itsColumnList[i].doCol(tableType,flagWCS)) this->itsColumnList[i].printTitle(stream);
      stream << "\n"<<this->itsCommentString;
      for(size_t i=0;i<this->itsColumnList.size();i++)
	if(this->itsColumnList[i].doCol(tableType,flagWCS)) this->itsColumnList[i].printUnits(stream);
      stream << "\n"<<this->itsCommentString;
      for(size_t i=0;i<this->itsColumnList.size();i++)
	if(this->itsColumnList[i].doCol(tableType,flagWCS)) this->itsColumnList[i].printDash(stream);
      stream << "\n";

      for(size_t i=0;i<this->itsCommentString.size();i++) this->itsColumnList[0].widen();
    }


      template <class T> void CatalogueSpecification::check(std::string type, T value)
      {
          if(hasColumn(type)){
              itsColumnList[itsTypeMap[type]].check(value);
          }
      }
      template void CatalogueSpecification::check<int>(std::string type, int value);
      template void CatalogueSpecification::check<long>(std::string type, long value);
      template void CatalogueSpecification::check<unsigned int>(std::string type, unsigned int value);
      template void CatalogueSpecification::check<unsigned long>(std::string type, unsigned long value);
      template void CatalogueSpecification::check<std::string>(std::string type, std::string value);
      template void CatalogueSpecification::check<float>(std::string type, float value);
      template void CatalogueSpecification::check<double>(std::string type, double value);
      
      template <class T> void CatalogueSpecification::check(std::string type, T value, bool doPrec)
      {
          if(hasColumn(type)){
              itsColumnList[itsTypeMap[type]].check(value,doPrec);
          }
      }
      template void CatalogueSpecification::check<float>(std::string type, float value, bool doPrec);
      template void CatalogueSpecification::check<double>(std::string type, double value, bool doPrec);

      void CatalogueSpecification::checkAll(std::vector<Detection> &objectList, FitsHeader &head)
      {
          // Now test each object against each new column, ensuring each
          // column has sufficient width and (in most cases) precision to
          // accomodate the data.
          std::vector<Detection>::iterator obj;
          for(obj = objectList.begin(); obj < objectList.end(); obj++){

              this->check("NUM",obj->getID());
              this->check("NAME",obj->getName());
              this->check("X",obj->getXcentre()+obj->getXOffset());
              this->check("Y",obj->getYcentre()+obj->getYOffset());
              this->check("Z",obj->getZcentre()+obj->getZOffset());
              if(head.isWCS()){
                  this->check("RA",obj->getRAs());
                  this->check("DEC",obj->getDecs());
                  this->check("RAJD",obj->getRA());
                  this->check("DECJD",obj->getDec());
                  if(head.canUseThirdAxis()){
                      this->check("VEL",obj->getVel());
                  }
                  this->check("MAJ",obj->getMajorAxis());
                  this->check("MIN",obj->getMinorAxis());
                  // For the PA column, we don't increase the precision. If
                  // something is very close to zero position angle, then
                  // we're happy to call it zero.
                  this->check("PA",obj->getPositionAngle(),false);
                  this->check("WRA",obj->getRAWidth());
                  this->check("WDEC",obj->getDecWidth());
                  if(head.canUseThirdAxis()){
                      this->check("W50",obj->getW50());
                      this->check("W20",obj->getW20());
                      this->check("WVEL",obj->getVelWidth());
                  }
	    
                  this->check("FINT",obj->getIntegFlux());
                  if(obj->getIntegFluxError()>0.)
                      this->check("FINTERR",obj->getIntegFluxError());
              }
              this->check("FTOT",obj->getTotalFlux());
              if(obj->getTotalFluxError()>0.)
                  this->check("FTOTERR",obj->getTotalFluxError());
              this->check("FPEAK",obj->getPeakFlux());
              if(obj->getPeakSNR()>0.)
                  this->check("SNRPEAK",obj->getPeakSNR());
              this->check("X1",obj->getXmin()+obj->getXOffset());
              this->check("X2",obj->getXmax()+obj->getXOffset());
              this->check("Y1",obj->getYmin()+obj->getYOffset());
              this->check("Y2",obj->getYmax()+obj->getYOffset());
              this->check("Z1",obj->getZmin()+obj->getZOffset());
              this->check("Z2",obj->getZmax()+obj->getZOffset());
              this->check("NVOX",obj->getSize());
              this->check("XAV",obj->getXaverage()+obj->getXOffset());
              this->check("YAV",obj->getYaverage()+obj->getYOffset());
              this->check("ZAV",obj->getZaverage()+obj->getZOffset());
              this->check("XCENTROID",obj->getXCentroid()+obj->getXOffset());
              this->check("YCENTROID",obj->getYCentroid()+obj->getYOffset());
              this->check("ZCENTROID",obj->getZCentroid()+obj->getZOffset());
              this->check("XPEAK",obj->getXPeak()+obj->getXOffset());
              this->check("YPEAK",obj->getYPeak()+obj->getYOffset());
              this->check("ZPEAK",obj->getZPeak()+obj->getZOffset());
              this->check("NUMCH",obj->getNumChannels());
              this->check("SPATSIZE",obj->getSpatialSize());

          }


      }


  }

}
