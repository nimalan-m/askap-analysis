add_executable(textraction textraction.cc)
target_link_libraries(textraction 
	askap::analysis
	${CPPUNIT_LIBRARY}
)
target_compile_definitions(textraction PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)

add_test(
	NAME textraction
	COMMAND textraction
	)
