add_executable(tpatternmatching tpatternmatching.cc)
target_link_libraries(tpatternmatching 
	askap::analysis
	${CPPUNIT_LIBRARY}
)
target_compile_definitions(tpatternmatching PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)

add_test(
	NAME tpatternmatching
	COMMAND tpatternmatching
	)
