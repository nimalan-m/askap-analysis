add_executable(tsourcefitting tsourcefitting.cc)
target_link_libraries(tsourcefitting 
	askap::analysis
	${CPPUNIT_LIBRARY}
)
target_compile_definitions(tsourcefitting PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)

add_test(
	NAME tsourcefitting
	COMMAND tsourcefitting
	)
