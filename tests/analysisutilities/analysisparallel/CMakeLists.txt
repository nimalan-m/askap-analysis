add_executable(tanalysisparallel tanalysisparallel.cc)
target_link_libraries(tanalysisparallel 
	askap::analysis
	${CPPUNIT_LIBRARY}
)
target_compile_definitions(tanalysisparallel PUBLIC
        casa=casacore
        HAVE_AIPSPP
        HAVE_BOOST
        HAVE_LOG4CXX
)

add_test(
	NAME tanalysisparallel
	COMMAND tanalysisparallel
	)
